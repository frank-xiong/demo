package frank.cloud.demo.server.controller;

import frank.cloud.demo.api.s2.DemoClient2;
import frank.cloud.demo.commons.InternetUtils;
import frank.cloud.demo.server.ServerApplication1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController1 {

    private static final Logger logger = LoggerFactory.getLogger(DemoController1.class);

    @Autowired
    private DemoClient2 demoClient;

    @GetMapping("/inner/s1/info")
    public String innerInfo() {
        logger.info("call /inner/s1/info");
        return InternetUtils.makeServerInfo("I'm server-1");
    }

    @GetMapping("/call/s1/info")
    public String serverInfo() {
        logger.info("call /call/s1/info");
        return innerInfo();
    }

    @GetMapping("/call/s1/to/s2")
    public String fromOther() {
        logger.info("call /call/s1/to/s2");
        return demoClient.innerInfo();
    }

    @GetMapping("/call/s1/plus")
    public Integer plus(@RequestParam Integer a, @RequestParam Integer b) {
        logger.info("call /call/s1/plus");
        return a + b;
    }

}
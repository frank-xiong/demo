package frank.cloud.demo.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(
        {
                "frank.cloud.demo.api.s2"
        }
)
@ComponentScan({ "frank.cloud.demo.server" })
public class ServerApplication1 {

    private static final Logger logger = LoggerFactory.getLogger(ServerApplication1.class);

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication1.class, args);
        logger.info("ServerApplication1 started !");
    }

}

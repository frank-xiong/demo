package frank.cloud.demo.server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.NetworkInterface;

@RestController
public class DemoController3 {

    private static final Logger logger = LoggerFactory.getLogger(DemoController3.class);

    @GetMapping("/plus")
    public Integer plus(@RequestParam Integer a, @RequestParam Integer b) {
        Integer r = a + b;
        logger.info("call plus {} + {}: {}", a, b, r);
        return r;
    }

    @GetMapping("/host-info")
    public String hostInfo() {
        try {
            InetAddress ia = InetAddress.getLocalHost();
            byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();

            logger.info("call show host info");
            return " hostname: " + ia.getHostName()
                    + ", ip: " + ia.getHostAddress()
                    + ", mac: " + convertMacAddress(mac);
        } catch (Throwable t) {
            logger.error("get host info occur exception:", t);
            return "get host info occur exception !";
        }
    }

    public static String convertMacAddress(byte[] mac) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mac.length; i++) {
            if (i != 0) {
                sb.append("-");
            }
            String s = Integer.toHexString(mac[i] & 0xFF);
            if (s.length() < 2) {
                sb.append("0");
            }
            sb.append(s);
        }

        return sb.toString().toUpperCase();
    }
}

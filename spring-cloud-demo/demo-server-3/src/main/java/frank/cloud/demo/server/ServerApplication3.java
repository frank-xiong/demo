package frank.cloud.demo.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerApplication3 {

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication3.class, args);
        System.out.println("ServerApplication3 started");
    }

}

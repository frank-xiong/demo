package frank.cloud.demo.api.s2;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "sc-server2", url = "demo-service-2:9002")
public interface DemoClient2 {

    @GetMapping("/inner/s2/info")
    String innerInfo();

}
package frank.cloud.demo.gateway.controller;

import frank.cloud.demo.commons.InternetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GatewayController {

    private static final Logger logger = LoggerFactory.getLogger(GatewayController.class);

    @GetMapping("/call/gateway/info")
    public String serverInfo() {
        logger.info("call /call/gateway/info");
        return InternetUtils.makeServerInfo("I'm gateway");
    }

}
package frank.cloud.demo.commons;

import frank.cloud.demo.commons.model.HostInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.NetworkInterface;

public class InternetUtils {

    private static final Logger logger = LoggerFactory.getLogger(InternetUtils.class);

    public static String getHostname() {
        try {
            InetAddress ia = InetAddress.getLocalHost();
            return ia.getHostName();
        } catch (Throwable t) {
            logger.error("get host name occur exception:", t);
            return "get host name occur exception !";
        }
    }

    public static String getIP() {
        try {
            InetAddress ia = InetAddress.getLocalHost();
            return ia.getHostAddress();
        } catch (Throwable t) {
            logger.error("get host address occur exception:", t);
            return "get host address occur exception !";
        }
    }

    public static String getMacAddress() {
        try {
            InetAddress ia = InetAddress.getLocalHost();
            byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();

            return convertMacAddress(mac);
        } catch (Throwable t) {
            logger.error("get mac address occur exception:", t);
            return "get mac address occur exception !";
        }
    }

    public static HostInfo getHostInfo() {
        try {
            InetAddress ia = InetAddress.getLocalHost();
            byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();

            HostInfo info = new HostInfo();
            info.setHostname(ia.getHostName());
            info.setInnerIP(ia.getHostAddress());
            info.setMacAddress(convertMacAddress(mac));

            return info;
        } catch (Throwable t) {
            logger.error("get host info occur exception:", t);
            return null;
        }
    }

    public static String convertMacAddress(byte[] mac) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mac.length; i++) {
            if (i != 0) {
                sb.append("-");
            }
            String s = Integer.toHexString(mac[i] & 0xFF);
            if (s.length() < 2) {
                sb.append("0");
            }
            sb.append(s);
        }

        return sb.toString().toUpperCase();
    }

    public static String makeServerInfo(String serverName) {
        HostInfo info = getHostInfo();
        if (info == null) {
            return "get host info failed !";
        }

        return "server name: " + serverName
                + "\nhost name: " + info.getHostname()
                + "\nInner IP: " + info.getInnerIP()
                + "\nMAC Address: " + info.getMacAddress();
    }

}

package frank.cloud.demo.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(
        {
                "frank.cloud.demo.api.s1"
        }
)
@ComponentScan({ "frank.cloud.demo.server" })
public class ServerApplication2 {

    private static final Logger logger = LoggerFactory.getLogger(ServerApplication2.class);

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication2.class, args);
        logger.info("ServerApplication2 started !");
    }

}

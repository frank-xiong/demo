package frank.cloud.demo.server.controller;

import frank.cloud.demo.api.s1.DemoClient1;
import frank.cloud.demo.commons.InternetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController2 {

    private static final Logger logger = LoggerFactory.getLogger(DemoController2.class);

    @Autowired
    private DemoClient1 demoClient;

    @GetMapping("/inner/s2/info")
    public String innerInfo() {
        logger.info("call /inner/s2/info");
        return InternetUtils.makeServerInfo("I'm server-2");
    }

    @GetMapping("/call/s2/info")
    public String serverInfo() {
        logger.info("call /call/s2/info");
        return innerInfo();
    }

    @GetMapping("/call/s2/to/s1")
    public String fromOther() {
        logger.info("call /call/s2/to/s1");
        return demoClient.innerInfo();
    }

    @GetMapping("/call/s2/plus")
    public Integer plus(@RequestParam Integer a, @RequestParam Integer b) {
        logger.info("call /call/s2/plus");
        return a + b;
    }

}
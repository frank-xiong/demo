package frank.cloud.demo.api.s1;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "sc-server1", url = "demo-service-1:9001")
public interface DemoClient1 {

    @GetMapping("/inner/s1/info")
    String innerInfo();

}